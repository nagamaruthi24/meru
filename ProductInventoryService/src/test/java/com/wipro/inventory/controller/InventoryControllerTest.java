/**
 * 
 */
package com.wipro.inventory.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.inventory.model.InventoryDetails;

/**
 * @author SPADAKULA
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class InventoryControllerTest {
    
    @Autowired
    private MockMvc mvc;
    
     @Autowired
        private ObjectMapper objectMapper;
     
    @Test
    public final void testfindAll() throws Exception {
        MvcResult result = mvc.perform(get("/inventory").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andReturn();
        TypeReference<List<InventoryDetails>> typeReference = new TypeReference<List<InventoryDetails>>() {
        };
        List<InventoryDetails> inventories = objectMapper.readValue(result.getResponse().getContentAsByteArray(), typeReference);
        Assert.assertEquals(2,inventories.size());
      
    }
    
    @Test
    public final void testpostOneInventory() throws Exception {
        InventoryDetails details = InventoryDetails.builder().productId(12828l).name("name").price(100d).count(100)
                .description("description").build();
        mvc.perform(post("/inventory")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(
                        details
                )))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.productId").value(details.getProductId()));
    }

    @Test
    public final void testpostOneMandatory() throws Exception {
        InventoryDetails details = InventoryDetails.builder().name("name").price(100d).count(100)
                .description("description").build();
        MvcResult result = mvc.perform(post("/inventory")
                .contentType(MediaType.APPLICATION_JSON)
                
                .content(objectMapper.writeValueAsString(
                        details
                )))
                .andExpect(status().isCreated())
                .andReturn();
        //String res =  objectMapper.readValue(result.getResponse().getContentAsString(), String.class);
        //Assert.assertEquals("Product Id mandatory", result.getResponse().getContentAsString());
    }
    

}
