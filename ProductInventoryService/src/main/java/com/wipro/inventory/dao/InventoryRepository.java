/**
 * 
 */
package com.wipro.inventory.dao;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.inventory.model.InventoryDetails;

/**
 * @author SPadakula
 *
 */
@Repository
@Transactional
public interface InventoryRepository extends JpaRepository<InventoryDetails, Long> {

    public Optional<List<InventoryDetails>> findByProductId(Long productId);

    public void deleteByProductId(Long productId);

}
